<?php






// 1) recuperer valeur tableau

// 2) vérifier que l'utilisateur n'a pas deja participé

// 3) si ok, on écrit dans un fichier .txt global les valeurs saisies

// 4) fin du script


$contenuPanier = $_POST['contenuPanier'];

if(!$contenuPanier) {
    $response = array ('response' => 'error');
    echo json_encode($response);
    exit;
}

$cookie_name = 'deforestation_participation_verification';

if(isset($_COOKIE[$cookie_name])) {
    $response = array ('response' => 'participated');
    echo json_encode($response);
    exit;
}
else {


    $file = fopen('assets/resultats.txt', 'a+');

    $lines_array = file("assets/resultats.txt");

    $newContenuPanier = array(
        "pin" => 0,
        "cornouiller" => 0,
        "chene" => 0,
        "margousier" => 0,
        "olivier" => 0,
        "faidherbia" => 0
    );


    foreach($contenuPanier as $key => $var) {

        $search_string = $key;

        foreach($lines_array as $line) {
            if(strpos($line, $search_string) !== false) {
                list(, $currentValue) = explode(":", $line);
                $currentValue = intval(trim($currentValue));
            }
        }
        $newValue = $currentValue + $var;

        $newContenuPanier[$key] = $newValue;

    }

    file_put_contents('assets/resultats.txt', '');

    foreach ($newContenuPanier as $key => $val)
    {
        fwrite($file, $key.": ".$val."\n");
        $i++;
    }
    fclose($file);


    $response = array ('response' => 'success');
    echo json_encode($response);
    setcookie($cookie_name, 1, time() + (86400 * 30), "/"); // 30 days
}
